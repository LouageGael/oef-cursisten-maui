﻿namespace oef_Cursisten_les_17_maui;

public partial class MainPage : ContentPage
{

    private List<Cursist> lijstCursisten = new List<Cursist>();
    private Cursist cursist = null;
	public MainPage()
	{
		InitializeComponent();
	}
    private void onBtnToevoegenClicked(object sender, EventArgs e)
    {
        if(!string.IsNullOrEmpty(txtFamilienaam.Text) && !string.IsNullOrEmpty(txtVoornaam.Text))
        {

            if(!lijstCursisten.Any(x => x.Familienaam.ToLower() == txtFamilienaam.Text.ToLower() && x.Voornaam.ToLower() == txtVoornaam.Text.ToLower()))
            {
                lijstCursisten.Add(new Cursist
                {
                    Voornaam = txtVoornaam.Text,
                    Familienaam = txtFamilienaam.Text
                });
                lvCursisten.ItemsSource = null;
                lvCursisten.ItemsSource = lijstCursisten;
                lblInfo.Text = String.Empty;
                lblInfo.BackgroundColor = Color.FromArgb("#FFFFFF");
                txtFamilienaam.Text = String.Empty;
                txtVoornaam.Text = String.Empty;
            } else
            {
                lblInfo.Text = "Cursist al in lijst.";
                lblInfo.BackgroundColor = Color.FromArgb("#FF0000");
            }
        } else
        {
            lblInfo.Text = "Niet alle tekstvelden zijn ingevuld.";
            lblInfo.BackgroundColor = Color.FromArgb("#FF0000");
        }

    }
    private void onBtnVerwijderenClicked(object sender, EventArgs e)
    {
        if(lvCursisten.SelectedItem != null && cursist != null) 
        {
            lijstCursisten.Remove(cursist);
            lvCursisten.ItemsSource = null;
            lvCursisten.ItemsSource = lijstCursisten;
        }
    }
    private void onBtnSluitenClicked(object sender, EventArgs e)
    {
        Environment.Exit(0);
    }
    private void OnCursistSelected(object sender, SelectedItemChangedEventArgs e)
    {
        cursist = lvCursisten.SelectedItem as Cursist;
    }
}
class Cursist
{
	private string _familienaam;
	private string _voornaam;
    public string Familienaam { get => _familienaam; set => _familienaam = UpperCaseNames(value); }
    public string Voornaam { get => _voornaam; set => _voornaam = UpperCaseNames(value); }
	public string FullName => $"{Familienaam}  {Voornaam}";
    private string UpperCaseNames(string name)
    {
        string result = "";
        if (name.Contains(' ') && name[name.Length - 1] != ' ' && !name.Contains("  "))
        {
            string[] splitName = name.Split(' ');
            for (int i = 0; i < splitName.Length; i++)
            {
                result += splitName[i][0].ToString().ToUpper();
                for (int j = 1; j < splitName[i].Length; j++)
                {
                    result += splitName[i][j];
                }
                result += " ";
            }
        }
        else
        {
            result += name[0].ToString().ToUpper();
            for (int i = 1; i < name.Length; i++)
            {
                result += name[i];
            }
        }
        return result;
    }
}

